This is blender compiled with ffmpeg support for fedora/nobara:

Note, you -will- need rpmfusion repositories enabled for ffmpeg:

https://rpmfusion.org/

If you do not want to build this, I host a fedora-compatible repository for it here:

`sudo vim /etc/yum.repos.d/blender-nobara.repo`

```
[blender-nobara]
name=blender-nobara
baseurl=https://www.nobaraproject.org/repo/nobara/$releasever/$basearch/
type=rpm-md
skip_if_unavailable=True
gpgcheck=0
repo_gpgcheck=0
enabled=1
enabled_metadata=1
```

Then update with:

`sudo dnf update blender --refresh`

or install with:

`sudo dnf install blender`


Build instructions:
```
$ sudo dnf install mock pykickstart
$ sudo usermod -a -G mock <user>
$ mock -r /etc/mock --init
$ sudo setenforce 0
$ mock -r nobara-35-x86_64.cfg --rebuild blender*.fc35.src.rpm
$ mv /var/lib/mock/nobara-35-x86_64/result/*.rpm /path/to/some/folder/
$ exit
$ mock -r nobara-35-x86_64.cfg --clean
$ sudo setenforce 1
```
Finished! Packages will be in whatever folder you chose for /path/to/some/folder/

Install instructions:
```
$ cd /path/to/some/folder/
$ yum install blender*.rpm
```

Upgrade instructions:
```
$ cd /path/to/some/folder/
$ yum upgrade blender*.rpm
```

